# Simple weather-app

Learn to use API calls for a CLI application using Python standard library.

Learning resource: https://realpython.com/build-a-python-weather-app-cli/

## Usage
```terminal
python weather.py CITY [-i, --imperial]
```

Use the optional flag to get the temperature of the selected city in Fahrenheit. (Aka the worst temperature scale).

## Requirements
* Python (version 3.8.12 was used in development)
* Registered account at https://openweathermap.org/
    * Once registered find your API key at https://home.openweathermap.org/api_keys 
    * Place the key in a file named "secrets.ini". It should look like this:
            
                [openweather] 
                api_key="YOUR-API-KEY-HERE"
        